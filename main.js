// Get Use Input
let filterInput = document.getElementById('filterInput');

// Add event listener
filterInput.addEventListener('keyup',() => {
    // Value of input
    let filterValue = filterInput.value.toUpperCase();

    // console.log(filterValue);

    // Get names list
    let namesList = document.getElementById('names');
    // Get list-items
    let listItems = namesList.querySelectorAll('li.collection-item');

    // console.log(listItems);

    // Loop through the listItems
    for(let i = 0; i < listItems.length; i++){
        let listLink = listItems[i].getElementsByTagName('a')[0];

        // If Matched
        if(listLink.innerHTML.toUpperCase().indexOf(filterValue) > -1){
            listItems[i].style.display = '';
        } else {
            listItems[i].style.display = 'none';
        }
    }
});